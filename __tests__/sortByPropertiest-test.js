import sortByProperties from "../src/components/actions/sort/sortByProperties";

describe("Функция sortByProperties", () => {
  const elements = [
    {
      "name": "item_4",
      "flags": ["flower", "heart", "sun", "flash"]
    },
    {
      "name": "item_1",
      "flags": ["flower", "heart", "sun", "flash"]
    },
    {
      "name": "item_3",
      "flags": ["flower", "heart", "sun", "flash"]
    },

    {
      "name": "item_5",
      "flags": ["flower", "heart", "sun"]
    },
    {
      "name": "item_2",
      "flags": ["flower", "heart", "sun", "flash"]
    },
  ];
  const properties = ["flash"];
  const result = [
    {
      "name": "item_4",
      "flags": ["flower", "heart", "sun", "flash"]
    },
    {
      "name": "item_1",
      "flags": ["flower", "heart", "sun", "flash"]
    },
    {
      "name": "item_3",
      "flags": ["flower", "heart", "sun", "flash"]
    },
    {
      "name": "item_2",
      "flags": ["flower", "heart", "sun", "flash"]
    },
  ];
  const secondResult = [
    {
      "name": "item_4",
      "flags": ["flower", "heart", "sun", "flash"]
    },
    {
      "name": "item_1",
      "flags": ["flower", "heart", "sun", "flash"]
    },
    {
      "name": "item_3",
      "flags": ["flower", "heart", "sun", "flash"]
    },
    {
      "name": "item_2",
      "flags": ["flower", "heart", "sun", "flash"]
    },
  ];
  test("все с флагом flash", () => {
    expect(sortByProperties({ elements, properties })).toEqual(result)
  });
  test("пустое занчение properties", () => {
    expect(sortByProperties({ elements, properties: [] })).toEqual(elements)
  });
  test("все с флагами flash и sun", () => {
    expect(sortByProperties({ elements, properties: ["flash", "sun"] })).toEqual(secondResult)
  });
});
