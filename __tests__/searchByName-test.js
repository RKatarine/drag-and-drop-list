import { searchElements } from "../src/components/actions/search";

describe("Функция searchElements", () => {
  const elements = [
    {
      "name": "item_4",
      "flags": ["flower", "heart", "sun", "flash"]
    },
    {
      "name": "item_1",
      "flags": ["flower", "heart", "sun", "flash"]
    },
    {
      "name": "item_3",
      "flags": ["flower", "heart", "sun", "flash"]
    },

    {
      "name": "item_5",
      "flags": ["flower", "heart", "sun"]
    },
    {
      "name": "item_2",
      "flags": ["flower", "heart", "sun", "flash"]
    },
  ];
  const result = [
    {
      "name": "item_1",
      "flags": ["flower", "heart", "sun", "flash"]
    }
  ];
  test("поиск без пробела", () => {
    expect(searchElements("1", elements)).toEqual(result)
  });
  test("поиск c пробелом", () => {
    expect(searchElements("i 1", elements)).toEqual(result)
  });
});
