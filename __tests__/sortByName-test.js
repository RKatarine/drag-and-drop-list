import { alphabet, alphabetRevers } from "../src/components/actions/sort/sortByName";

describe("Функция sortByName", () => {
  const elements = [
    {
      "name": "item_4",
      "flags": ["flower", "heart", "sun", "flash"]
    },
    {
      "name": "item_1",
      "flags": ["flower", "heart", "sun", "flash"]
    },
    {
      "name": "item_3",
      "flags": ["flower", "heart", "sun", "flash"]
    },

    {
      "name": "item_5",
      "flags": ["flower", "heart", "sun"]
    },
    {
      "name": "item_2",
      "flags": ["flower", "heart", "sun", "flash"]
    },
  ];
  const resultAlphabet = [
    {
      "name": "item_1",
      "flags": ["flower", "heart", "sun", "flash"]
    },
    {
      "name": "item_2",
      "flags": ["flower", "heart", "sun", "flash"]
    },
    {
      "name": "item_3",
      "flags": ["flower", "heart", "sun", "flash"]
    },
    {
      "name": "item_4",
      "flags": ["flower", "heart", "sun", "flash"]
    },
    {
      "name": "item_5",
      "flags": ["flower", "heart", "sun"]
    }
  ];
  const resultAlphabetRevers = [
    {
      "name": "item_5",
      "flags": ["flower", "heart", "sun"]
    },
    {
      "name": "item_4",
      "flags": ["flower", "heart", "sun", "flash"]
    },
    {
      "name": "item_3",
      "flags": ["flower", "heart", "sun", "flash"]
    },
    {
      "name": "item_2",
      "flags": ["flower", "heart", "sun", "flash"]
    },
    {
      "name": "item_1",
      "flags": ["flower", "heart", "sun", "flash"]
    }
  ];
  test("в прямом порядке по алфавиту", () => {
    expect(alphabet({elements})).toEqual(resultAlphabet)
  });
  test("в обратном порядке по алфавиту", () => {
    expect(alphabetRevers({elements})).toEqual(resultAlphabetRevers)
  });
});
