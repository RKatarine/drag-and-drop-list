const mainColor = "#424242";
const white = "#ffffff";
const secondColor = "#2196f3";
const lightSecondColor = "#e3f2fd";
const opacity = "#ffffff00";

export {
  mainColor,
  white,
  secondColor,
  lightSecondColor,
  opacity
};
