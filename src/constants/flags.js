import Flower from "@material-ui/icons/LocalFlorist";
import Heart from "@material-ui/icons/Favorite";
import Sun from "@material-ui/icons/WbSunny";
import Flash from "@material-ui/icons/FlashOn";

const flags = ["flower", "heart", "sun", "flash"];

const dictFeature = {
  "flower": Flower,
  "heart": Heart,
  "sun": Sun,
  "flash": Flash,
};

export {
  flags,
  dictFeature,
}