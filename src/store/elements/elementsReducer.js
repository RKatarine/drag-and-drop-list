import createReducer from "../createReducer";

import {
  GET_LEFT_ELEMENTS_SUCCESS,
  GET_LEFT_ELEMENTS_FAILURE,
  GET_LEFT_ELEMENTS_REQUEST,
  GET_RIGHT_ELEMENTS_SUCCESS,
  GET_RIGHT_ELEMENTS_FAILURE,
  GET_RIGHT_ELEMENTS_REQUEST,
  SELECT_ELEMENT_SUCCESS,
  GET_SEARCH_RIGHT_ELEMENTS_SUCCESS,
  GET_SEARCH_RIGHT_ELEMENTS_REQUEST,
  GET_SEARCH_LEFT_ELEMENTS_SUCCESS,
  GET_SEARCH_LEFT_ELEMENTS_REQUEST,
  SET_SORTED_LEFT_ELEMENTS,
  SET_SORTED_RIGHT_ELEMENTS,
  SET_UPDATED_LISTS_ELEMENTS,
} from "./elementsConstants";

const initialState = {
  statusText: '',
  selectedId: '',
  selectedItem: undefined,
  leftElements: [],
  rightElements: [],
  leftElementsForSearch: [],
  rightElementsForSearch: [],
  sortLeftElements: true,
  propertiesForSort: [],
  queryText: '',
};


export default createReducer(initialState, {
  [GET_LEFT_ELEMENTS_SUCCESS]: (state, payload) => Object.assign({}, state, {
    leftElements: payload,
    leftElementsForSearch: payload,
  }),
  [GET_LEFT_ELEMENTS_FAILURE]: (state, payload) => Object.assign({}, state, {
    leftElements: [],
    leftElementsForSearch: [],
    statusText: payload,
  }),
  [GET_LEFT_ELEMENTS_REQUEST]: (state, payload) => Object.assign({}, state, {
    statusText: "Запрос на получение элементов списка",
  }),
  [GET_RIGHT_ELEMENTS_SUCCESS]: (state, payload) => Object.assign({}, state, {
    rightElements: payload,
    rightElementsForSearch: payload,
  }),
  [GET_RIGHT_ELEMENTS_FAILURE]: (state, payload) => Object.assign({}, state, {
    rightElements: [],
    rightElementsForSearch: [],
    statusText: payload,
  }),
  [GET_RIGHT_ELEMENTS_REQUEST]: (state, payload) => Object.assign({}, state, {
    statusText: "Запрос на получение элементов списка",
  }),
  [SELECT_ELEMENT_SUCCESS]: (state, payload) => Object.assign({}, state, {
    selectedItem: payload.selectedItem,
    selectedId: payload.selectedId,
  }),
  [GET_SEARCH_RIGHT_ELEMENTS_SUCCESS]: (state, payload) => Object.assign({}, state, {
    rightElements: payload.rightElements,
    queryText: payload.queryText,
  }),
  [GET_SEARCH_RIGHT_ELEMENTS_REQUEST]: (state, payload) => Object.assign({}, state, {
    statusText: "Поисковой запрос правая панель",
  }),
  [GET_SEARCH_LEFT_ELEMENTS_SUCCESS]: (state, payload) => Object.assign({}, state, {
    leftElements: payload.leftElements,
    queryText: payload.queryText,
  }),
  [GET_SEARCH_LEFT_ELEMENTS_REQUEST]: (state, payload) => Object.assign({}, state, {
    statusText: "Поисковой запрос правая панель",
  }),
  [SET_SORTED_LEFT_ELEMENTS]: (state, payload) => Object.assign({}, state, {
    leftElements: payload.sortedLeftElements,
    sortLeftElements: payload.sortLeftElements,
  }),
  [SET_SORTED_RIGHT_ELEMENTS]: (state, payload) => Object.assign({}, state, {
    rightElements: payload.sortedRightElements,
    propertiesForSort: payload.propertiesForSort,
  }),
  [SET_UPDATED_LISTS_ELEMENTS]: (state, payload) => Object.assign({}, state, {
    rightElements: payload.rightElements,
    leftElements: payload.leftElements,
    rightElementsForSearch: payload.rightElements,
    leftElementsForSearch: payload.leftElements,
    sortLeftElements: true,
    propertiesForSort: [],
    queryText:'',
  }),
});

