import api from "../../api";

import {
  GET_LEFT_ELEMENTS_SUCCESS,
  GET_LEFT_ELEMENTS_FAILURE,
  GET_LEFT_ELEMENTS_REQUEST,
  GET_RIGHT_ELEMENTS_SUCCESS,
  GET_RIGHT_ELEMENTS_FAILURE,
  GET_RIGHT_ELEMENTS_REQUEST,
  SELECT_ELEMENT_SUCCESS,
  GET_SEARCH_RIGHT_ELEMENTS_SUCCESS,
  GET_SEARCH_RIGHT_ELEMENTS_REQUEST,
  GET_SEARCH_LEFT_ELEMENTS_SUCCESS,
  GET_SEARCH_LEFT_ELEMENTS_REQUEST,
  SET_SORTED_LEFT_ELEMENTS,
  SET_SORTED_RIGHT_ELEMENTS,
  SET_UPDATED_LISTS_ELEMENTS,
} from "./elementsConstants";


function getLeftElementsRequest() {
  return {
    type: GET_LEFT_ELEMENTS_REQUEST,
  };
}

function getLeftElementsSuccess(data) {
  return {
    type: GET_LEFT_ELEMENTS_SUCCESS,
    payload: data,
  };
}

function getLeftElementsFailure(data) {
  return {
    type: GET_LEFT_ELEMENTS_FAILURE,
    payload: data,
  };
}

function getLeftElements() {
  getLeftElementsRequest();
  return dispatch => (api.elements.getAllLeft().then((response) => {
    const responseData = response.data;
    if (responseData.status) {
      const elementsData = responseData.data;
      dispatch(getLeftElementsSuccess(elementsData));
    } else {
      dispatch(getLeftElementsFailure(responseData.statusText));
    }
  }).catch(error => dispatch(getLeftElementsFailure(error))));
}


function getRightElementsRequest() {
  return {
    type: GET_RIGHT_ELEMENTS_REQUEST,
  };
}

function getRightElementsSuccess(data) {
  return {
    type: GET_RIGHT_ELEMENTS_SUCCESS,
    payload: data,
  };
}

function getRightElementsFailure(data) {
  return {
    type: GET_RIGHT_ELEMENTS_FAILURE,
    payload: data,
  };
}

function getRightElements() {
  getRightElementsRequest();
  return dispatch => (api.elements.getAllRight().then((response) => {
    const responseData = response.data;
    if (responseData.status) {
      const elementsData = responseData.data;
      dispatch(getRightElementsSuccess(elementsData));
    } else {
      dispatch(getRightElementsFailure(responseData.statusText));
    }
  }).catch(error => dispatch(getRightElementsFailure(error))));
}

function selectElementSuccess(data) {
  return {
    type: SELECT_ELEMENT_SUCCESS,
    payload: data,
  };
}

function selectElement({ selectedId, selectedItem }) {
  return (dispatch) => {
    dispatch(selectElementSuccess({ selectedId, selectedItem }));
  };
}

function searchLeftSuccess(data) {
  return {
    type: GET_SEARCH_LEFT_ELEMENTS_SUCCESS,
    payload: data,
  };
}

function searchLeftRequest() {
  return {
    type: GET_SEARCH_LEFT_ELEMENTS_REQUEST,
  };
}

export function searchLeft(leftElements, queryText) {
  searchLeftRequest();
  return dispatch => dispatch(searchLeftSuccess({leftElements, queryText}));
}


function searchRightSuccess(data) {
  return {
    type: GET_SEARCH_RIGHT_ELEMENTS_SUCCESS,
    payload: data,
  };
}

function searchRightRequest() {
  return {
    type: GET_SEARCH_RIGHT_ELEMENTS_REQUEST,
  };
}

export function searchRight(rightElements, queryText) {
  searchRightRequest();
  return dispatch => dispatch(searchRightSuccess({rightElements,queryText}));
}

function setSortLeftElements(data) {
  return {
    type: SET_SORTED_LEFT_ELEMENTS,
    payload: data,
  };
}

function sortLeftElements({ sortedLeftElements, sortLeftElements }) {
  return dispatch => dispatch(setSortLeftElements({ sortedLeftElements, sortLeftElements }));
}


function setSortRightElements(data) {
  return {
    type: SET_SORTED_RIGHT_ELEMENTS,
    payload: data,
  };
}

function sortRightElements({ sortedRightElements, propertiesForSort }) {
  return dispatch => dispatch(setSortRightElements({ sortedRightElements, propertiesForSort }));
}

function setUpdatedListsElements(data) {
  return {
    type: SET_UPDATED_LISTS_ELEMENTS,
    payload: data,
  };
}

function updateListsElements({ leftElements, rightElements }) {
  return dispatch => dispatch(setUpdatedListsElements({ leftElements, rightElements }));
}

export default {
  getLeftElements,
  getRightElements,
  selectElement,
  searchLeft,
  searchRight,
  sortLeftElements,
  sortRightElements,
  updateListsElements,
};
