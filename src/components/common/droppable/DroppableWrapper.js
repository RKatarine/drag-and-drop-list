import React from "react";
import PropTypes from "prop-types";
import { Droppable } from "react-beautiful-dnd";
import { opacity, secondColor } from "../../../constants/colors";

const getListStyle = isDraggingOver => ({
  background: isDraggingOver ? secondColor : opacity,
});

const DroppableWrapper = ({ droppableId, children }) => {
  return (
    <Droppable droppableId={droppableId}>
      {(provided, snapshot) => (
        <div
          ref={provided.innerRef}
          style={getListStyle(snapshot.isDraggingOver)}>
          {children}
          {provided.placeholder}
        </div>
      )}
    </Droppable>
  );

};
DroppableWrapper.propTypes = {
  children: PropTypes.node,
  droppableId: PropTypes.string.isRequired,
};

DroppableWrapper.defaultProps = {
  children: undefined,
};

export default DroppableWrapper;
