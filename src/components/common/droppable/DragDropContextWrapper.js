import React from "react";
import PropTypes from "prop-types";
import { DragDropContext } from "react-beautiful-dnd";

const DragDropContextWrapper = ({ dragActions: { onDragEnd }, children }) => {
  return (
    <DragDropContext onDragEnd={onDragEnd}>
      {children}
    </DragDropContext>
  );

};
DragDropContextWrapper.propTypes = {
  children: PropTypes.node,
  dragActions: PropTypes.shape({
    onDragEnd: PropTypes.func,
  }).isRequired,
};

DragDropContextWrapper.defaultProps = {
  children: undefined,
};

export default DragDropContextWrapper;
