import React from "react";
import PropTypes from "prop-types";
import { Draggable } from "react-beautiful-dnd";
import { secondColor, opacity } from "../../../constants/colors"

const grid = 8;

const getItemStyle = (isDragging, draggableStyle) => ({
  userSelect: "none",
  padding: grid,
  background: isDragging ? secondColor : opacity,
  ...draggableStyle
});

const DraggableWrapper = ({
                            draggableId,
                            index,
                            children
                          }) => {
  return (
    <Draggable
      key={draggableId}
      draggableId={draggableId}
      index={index}>
      {(provided, snapshot) => (
        <div
          ref={provided.innerRef}
          {...provided.draggableProps}
          {...provided.dragHandleProps}
          style={getItemStyle(
            snapshot.isDragging,
            provided.draggableProps.style
          )}>
          {children}
        </div>
      )}
    </Draggable>
  );

};
DraggableWrapper.propTypes = {
  children: PropTypes.node,
  draggableId: PropTypes.string.isRequired,
  index: PropTypes.number.isRequired,
};

DraggableWrapper.defaultProps = {
  children: undefined,
};

export default DraggableWrapper;
