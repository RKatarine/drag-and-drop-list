import React from "react";
import PropTypes from "prop-types";

import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles(theme => ({
  footer: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    flex: "0 0 auto",
    overflowY: "auto",
    paddingRight: theme.spacing(1),
  },
}));


const FooterWrapper = ({ children }) => {
  const classes = useStyles();
  return (
    <div className={classes.footer}>
        {children}
    </div>
  );

};
FooterWrapper.propTypes = {
  children: PropTypes.node,
};

FooterWrapper.defaultProps = {
  children: undefined,
};

export default FooterWrapper;
