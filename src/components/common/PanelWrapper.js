import React from "react";
import PropTypes from "prop-types";

import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles(theme => ({
  overflow: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "flex-start",
    alignItems: "center",
    flex: "1 0 auto",
    height: "100%",
    overflowY: "hidden",
    paddingRight: theme.spacing(1),
    border: "1px solid black"
  },
}));


const PanelWrapper = ({ children }) => {
  const classes = useStyles();
  return (
    <div className={classes.overflow}>
        {children}
    </div>
  );

};
PanelWrapper.propTypes = {
  children: PropTypes.node,
};

PanelWrapper.defaultProps = {
  children: undefined,
};

export default PanelWrapper;
