import React from "react";
import PropTypes from "prop-types";
import Default from "@material-ui/icons/RadioButtonUnchecked";

import { dictFeature } from "../../constants/flags";

const Flags = ({ features }) => {
  return features.map((feature) => {
    const Icon = dictFeature[feature] || Default;
    return <Icon key={feature}/>
  });
};

Flags.propTypes = {
  features: PropTypes.arrayOf(PropTypes.string)
};
Flags.defaultProps = {
  features: []
};
export default Flags;