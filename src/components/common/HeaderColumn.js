import React from "react";
import PropTypes from "prop-types";

import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles(theme => ({
  headerWrapper: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "flex-end",
    width: "100%",
  }
}));


const HeaderColumn = ({ children }) => {
  const classes = useStyles();
  return (
    <div className={classes.headerWrapper}>
      {children}
    </div>
  );

};
HeaderColumn.propTypes = {
  children: PropTypes.node,
};

HeaderColumn.defaultProps = {
  children: undefined,
};

export default HeaderColumn;
