import React from "react";
import PropTypes from "prop-types";

import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles(theme => ({
  overflow: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    overflowY: "hidden",
    height: "100%",
  }
}));


const PageWrapper = ({ children }) => {
  const classes = useStyles();
  return (
    <div className={classes.overflow}>
      {children}
    </div>
  );

};
PageWrapper.propTypes = {
  children: PropTypes.node,
};

PageWrapper.defaultProps = {
  children: undefined,
};

export default PageWrapper;
