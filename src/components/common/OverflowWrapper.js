import React from "react";
import PropTypes from "prop-types";

import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles(theme => ({
  overflow: {
    overflowY: "auto",
    width: "100%",
  },
}));


const OverflowWrapper = ({ children }) => {
  const classes = useStyles();
  return (
    <div className={classes.overflow}>
      {children}
    </div>
  );

};
OverflowWrapper.propTypes = {
  children: PropTypes.node,
};

OverflowWrapper.defaultProps = {
  children: undefined,
};

export default OverflowWrapper;
