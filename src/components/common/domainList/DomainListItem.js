import React, { Component } from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core";
import ListItem from "@material-ui/core/List";

import { lightSecondColor, mainColor, secondColor, white } from "../../../constants/colors";

const useStyles = makeStyles(theme => ({
  darkColor: {
    color: theme.palette.secondary.main
  },
  lightColor: { color: theme.palette.primary.main },
  backgroundColor: { color: theme.palette.primary.secondary },
  listItemBody: props => ({
    display: "flex",
    flexGrow: 1,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: props.backgroundColor
  }),
  firstRow: {
    display: "flex",
    flexGrow: 1,
    justifyContent: "center",
    alignItems: "center",
    padding: "8px 0px",
  },
  mainBar: {
    display: "flex",
    flexGrow: 1,
    alignItems: "center",
    overflow: "hidden"
  },
  title: props => ({
    fontSize: "14px",
    margin: "0 8px",
    textAlign: "center",
    color: props.titleColor
  }),
  actionBar: {
    display: "flex",
    alignItems: "center",
    justifyContent:"flex-end",
    minWidth:"96px"
  }
}));

const DomainListItem = ({
                          id,
                          title,
                          selected,
                          onClick,
                          actionBar,
                          level,
                        }) => {
  const titleColor = selected ? secondColor : mainColor;
  const backgroundColor = selected ? lightSecondColor : white;

  const classes = useStyles({
    backgroundColor,
    titleColor
  });

  const onClickItem = () => {
    if (onClick) {
      onClick(id);
    }
  };

  return (<ListItem
      key={id}
      id={id}
      disabled
      level={level}
    >
      <div className={classes.listItemBody} onClick={onClickItem}>
        <div className={classes.firstRow}>
          <div className={classes.mainBar}>
            <div className={classes.title}>
              {title}
            </div>
          </div>
          <div className={classes.actionBar}>
            {actionBar}
          </div>
        </div>
      </div>
    </ListItem>
  )

};
DomainListItem.propTypes = {
  id: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  selected: PropTypes.bool,
  onClick: PropTypes.func,
  actionBar: PropTypes.node,
  level: PropTypes.number,
};
DomainListItem.defaultProps = {
  selected: false,
  onClick: undefined,
  actionBar: undefined,
  level: 0,
};

export default DomainListItem