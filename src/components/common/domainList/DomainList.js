import React from "react";
import PropTypes from "prop-types";
import List from "@material-ui/core/List";
import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles(theme => ({
  widthList: {
    width: "100%",
  },
}));

const DomainList = ({ children }) => {
  const classes = useStyles();
  return (<List classes={{root:classes.widthList}}>
    {
      children
    }
  </List>)
};

DomainList.propTypes = {
  children: PropTypes.node,
};

DomainList.defaultProps = {
  children: undefined,
};

export default DomainList;
