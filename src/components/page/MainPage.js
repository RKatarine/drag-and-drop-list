import React from "react";
import PageWrapper from "../common/PageWrapper";
import DragDropContextWrapper from "../common/droppable/DragDropContextWrapper";
import LeftPanel from "../content/LeftPanel";
import CentralPanel from "../content/CentralPanel";
import RightPanel from "../content/RightPanel";
import addActions from "../actions/addActions";
import addDragAction from "../actions/addDragAction";


const LeftPanelWithActions = addActions(LeftPanel);
const RightPanelWithActions = addActions(RightPanel);
const DragDropContextWrapperWithActions = addDragAction(DragDropContextWrapper);

const MainPage = ({}) => (
  <PageWrapper>
    <DragDropContextWrapperWithActions>
      <LeftPanelWithActions/>
      <CentralPanel/>
      <RightPanelWithActions/>
    </DragDropContextWrapperWithActions>
  </PageWrapper>
);

export default MainPage;
