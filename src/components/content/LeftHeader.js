import React from "react";
import PropTypes from "prop-types";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import TextField from "@material-ui/core/TextField";
import HeaderColumn from "../common/HeaderColumn";
import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles(theme => ({
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: 200,
  },
}));

const LeftHeader = ({ queryText, sortLeftElements, sortByName, searchElement }) => {

  const classes = useStyles();

  const handleChangeSortation = event => {
    const checked = event.target.checked;
    console.log("handleChangeSortation", checked);
    if (sortByName) {
      sortByName(checked)
    }
  };
  const handleChangeSearch = event => {
    const query = event.target.value;
    if (searchElement) {
      searchElement(query, "left")
    }
  };
  return (
    <HeaderColumn>
      <FormControlLabel
        value="start"
        control={
          <Checkbox
            checked={sortLeftElements}
            onChange={handleChangeSortation}
            value="sortName"
            inputProps={{
              "aria-label": "primary checkbox",
            }}
          />
        }
        label="Sort"
        labelPlacement="start"
      />
      <TextField
        id="standard-name"
        label="Text filter"
        className={classes.textField}
        value={queryText}
        onChange={handleChangeSearch}
        margin="normal"
      />

    </HeaderColumn>
  );

};
LeftHeader.propTypes = {
  children: PropTypes.node,
  sortLeftElements: PropTypes.bool,
  searchElement: PropTypes.func,
  sortByName: PropTypes.func,
  queryText: PropTypes.string,
};

LeftHeader.defaultProps = {
  children: undefined,
  sortLeftElements: true,
  searchElement: undefined,
  sortByName: undefined,
  queryText: '',
};

export default LeftHeader;
