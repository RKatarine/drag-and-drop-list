import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";

import PanelWrapper from "../common/PanelWrapper";
import FooterWrapper from "../common/FooterWrapper";
import DomainListItem from "../common/domainList/DomainListItem";
import DomainList from "../common/domainList/DomainList";
import Flags from "../common/Flags";
import OverflowWrapper from "../common/OverflowWrapper";
import RightHeader from "../content/RightHeader";
import DroppableWrapper from "../common/droppable/DroppableWrapper";
import DraggableWrapper from "../common/droppable/DraggableWrapper";


class RightPanel extends Component {
  static propTypes = {
    elements: PropTypes.arrayOf(PropTypes
    .shape({ name: PropTypes.string, flags: PropTypes.array })),
    selectedId: PropTypes.string,
    commonActions: PropTypes.shape({
      getRightElements: PropTypes.func,
      selectRightElement: PropTypes.func,
      sortByProperties: PropTypes.func,
    }).isRequired,
  };
  static defaultProps = {
    elements: [],
    selectedId: ''
  };

  constructor(props) {
    super(props);
    this.selectElement = this.selectElement.bind(this);
    this.getList = this.getList.bind(this);
    this.getElementsListItems = this.getElementsListItems.bind(this);
  }

  componentDidMount() {
    const { commonActions } = this.props;
    commonActions.getRightElements();
  }

  selectElement(id) {
    const { commonActions } = this.props;
    commonActions.selectRightElement(id)
  }

  getElementsListItems(level) {
    const { rightElements, selectedId } = this.props;
    const currentLevel = level + 1;
    return rightElements.map((element, index) => {
      const { name, flags } = element;
      const id = `rightId&${index}`;
      const selected = (selectedId === id);
      return (
        <DraggableWrapper
          key={id}
          draggableId={id}
          index={index}
        >
          <DomainListItem
            key={id}
            id={id}
            title={name}
            level={currentLevel}
            selected={selected}
            onClick={this.selectElement}
            actionBar={
              <Flags features={flags}/>
            }
          />
        </DraggableWrapper>
       );
    });
  }

  getList() {
    const rootLevel = -1;
    return (<DomainList>
      {
        this.getElementsListItems(rootLevel)
      }
    </DomainList>)
  }

  render() {
    const { propertiesForSort, commonActions } = this.props;
    const { sortByProperties } = commonActions;
    return (
      <PanelWrapper>
        <RightHeader
          propertiesForSort={propertiesForSort}
          sortByProperties={sortByProperties}
        />
        <OverflowWrapper>
          <DroppableWrapper droppableId="right">
            {this.getList()}
          </DroppableWrapper>
        </OverflowWrapper>

        <FooterWrapper/>
      </PanelWrapper>
    );
  }
}

function mapStateToProps(state) {
  return {
    rightElementsForSearch: state.elements.rightElementsForSearch,
    rightElements: state.elements.rightElements,
    selectedId: state.elements.selectedId,
    propertiesForSort: state.elements.propertiesForSort,
  };
}

export default connect(mapStateToProps)(RightPanel);