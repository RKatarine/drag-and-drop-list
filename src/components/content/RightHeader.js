import React from "react";
import PropTypes from "prop-types";
import FormLabel from "@material-ui/core/FormLabel";
import FormControl from "@material-ui/core/FormControl";
import FormGroup from "@material-ui/core/FormGroup";
import Checkbox from "@material-ui/core/Checkbox";
import { makeStyles } from "@material-ui/core";
import Default from "@material-ui/icons/RadioButtonUnchecked"

import HeaderColumn from "../common/HeaderColumn";
import { dictFeature, flags } from "../../constants/flags";

const useStyles = makeStyles(theme => ({
  formControl: {
    margin: theme.spacing(1),
    marginTop: theme.spacing(3),
    flexDirection: "row",
    width: "100%",
    alignItems: "center",
    justifyContent: "space-between",
  },
  formGroup: {
    flexDirection: "row",
  }
}));

const RightHeader = ({ propertiesForSort, sortByProperties }) => {

  const classes = useStyles();

  const [selected, setSelected] = React.useState(propertiesForSort);

  const handleChangeFilter = name => event => {
    const selectedIndex = selected.indexOf(name);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, name);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      );
    }

    setSelected(newSelected);
    sortByProperties(newSelected);
  };

  const isSelected = name => selected.indexOf(name) !== -1;

  const getCheckboxes = () => {
    return flags.map((flag) => {
      const isItemSelected = isSelected(flag);
      const Icon = dictFeature[flag] || Default;
      return (
        <Checkbox
          key={flag}
          checked={isItemSelected}
          icon={<Icon/>}
          checkedIcon={<Icon/>}
          onChange={handleChangeFilter(flag)}
          value="sortName"
          inputProps={{
            "aria-label": "primary checkbox",
          }}
        />
      )
    })
  };

  return (
    <HeaderColumn>
      <FormControl className={classes.formControl}>
        <FormLabel>Filters</FormLabel>
        <FormGroup className={classes.formGroup}>
          {getCheckboxes()}
        </FormGroup>
      </FormControl>
    </HeaderColumn>
  );

};
RightHeader.propTypes = {
  propertiesForSort: PropTypes.arrayOf(PropTypes.string),
  sortByProperties: PropTypes.func,
};

RightHeader.defaultProps = {
  propertiesForSort: [],
  sortByProperties: undefined,
};

export default RightHeader;
