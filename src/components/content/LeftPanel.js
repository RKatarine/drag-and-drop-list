import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";

import PanelWrapper from "../common/PanelWrapper";
import DroppableWrapper from "../common/droppable/DroppableWrapper";
import DraggableWrapper from "../common/droppable/DraggableWrapper";
import FooterWrapper from "../common/FooterWrapper";
import DomainListItem from "../common/domainList/DomainListItem";
import DomainList from "../common/domainList/DomainList";
import Flags from "../common/Flags";
import OverflowWrapper from "../common/OverflowWrapper";
import LeftHeader from "./LeftHeader";


class LeftPanel extends Component {
  static propTypes = {
    elements: PropTypes.arrayOf(PropTypes
    .shape({ name: PropTypes.string, flags: PropTypes.array })),
    selectedId: PropTypes.string,
    commonActions: PropTypes.shape({
      getLeftElements: PropTypes.func,
      selectLeftElement: PropTypes.func,
      searchElement: PropTypes.func,
      sortByName: PropTypes.func,
    }).isRequired,
    sortLeftElements: PropTypes.bool,
  };
  static defaultProps = {
    elements: [],
    selectedId: '',
    sortLeftElements: true,
  };

  constructor(props) {
    super(props);
    this.selectElement = this.selectElement.bind(this);
    this.getList = this.getList.bind(this);
    this.getElementsListItems = this.getElementsListItems.bind(this);
  }

  componentDidMount() {
    const { commonActions } = this.props;
    commonActions.getLeftElements();
  }

  componentDidUpdate(prevProps) {
    const { leftElementsForSearch, sortLeftElements,leftElements, commonActions } = this.props;
    if (prevProps.leftElementsForSearch !== leftElementsForSearch
      && leftElementsForSearch.length !== 0 || leftElements.length !== prevProps.leftElements.length) {
      commonActions.sortByName(sortLeftElements);
    }
  }

  selectElement(id) {
    const { commonActions } = this.props;
    commonActions.selectLeftElement(id)
  }

  getElementsListItems(level) {
    const { leftElements, selectedId } = this.props;
    const currentLevel = level + 1;
    return leftElements.map((element, index) => {
      const { name, flags } = element;
      const id = `leftId&${index}`;
      const selected = (selectedId === id);
      return (
        <DraggableWrapper
          key={id}
          draggableId={id}
          index={index}
        >
          <DomainListItem
            key={id}
            id={id}
            title={name}
            level={currentLevel}
            selected={selected}
            onClick={this.selectElement}
            actionBar={
              <Flags features={flags}/>
            }
          />
        </DraggableWrapper>);
    });
  }

  getList() {
    const rootLevel = -1;
    return (<DomainList>
      {
        this.getElementsListItems(rootLevel)
      }
    </DomainList>)
  }

  render() {
    const { queryText, sortLeftElements, commonActions } = this.props;
    const { searchElement, sortByName } = commonActions;
    return (
      <PanelWrapper>
        <LeftHeader
          queryText={queryText}
          sortLeftElements={sortLeftElements}
          sortByName={sortByName}
          searchElement={searchElement}
        />
        <OverflowWrapper>
          <DroppableWrapper droppableId="left">
            {this.getList()}
          </DroppableWrapper>
        </OverflowWrapper>
        <FooterWrapper/>
      </PanelWrapper>
    );
  }
}

function mapStateToProps(state) {
  return {
    leftElementsForSearch: state.elements.leftElementsForSearch,
    leftElements: state.elements.leftElements,
    selectedId: state.elements.selectedId,
    sortLeftElements: state.elements.sortLeftElements,
    queryText: state.elements.queryText,
  };
}

export default connect(mapStateToProps)(LeftPanel);
