import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import Typography from "@material-ui/core/Typography";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import { makeStyles } from "@material-ui/core/styles";

import PanelWrapper from "../common/PanelWrapper";
import Flags from "../common/Flags";

const useStyles = makeStyles(theme => ({
  card: {
    marginTop: theme.spacing(2),
    minWidth: 275,
  },
  row:{
    display: "flex",
  },
  property:{
    paddingRight: theme.spacing(1),
  }
}));

const CentralPanel = ({ selectedItem }) => {

  const classes = useStyles();

  return (
    <PanelWrapper>
      <Card className={classes.card}>
        <CardContent>
          <Typography variant="h6" component="h6">INFO</Typography>
          {selectedItem && selectedItem.name ? <div className={classes.row}>
            <p className={classes.property}>Name:</p>
            <p>{selectedItem ? selectedItem.name : ''}</p>
          </div> : null}

          {selectedItem && selectedItem.flags ? <div className={classes.row}>
            <p className={classes.property}>Flags:</p>
            <p>{<Flags features={selectedItem.flags}/>}</p>
          </div> : null}
        </CardContent>
      </Card>
    </PanelWrapper>
  );

};
CentralPanel.propTypes = {
  selectedItem: PropTypes
  .shape({ name: PropTypes.string, flags: PropTypes.array }),
};

CentralPanel.defaultProps = {
  selectedItem: undefined,
};


function mapStateToProps(state) {
  return {
    selectedItem: state.elements.selectedItem,
  };
}

export default connect(mapStateToProps)(CentralPanel);