
const searchElements = function (query, elements) {
  if (!query.length) {
    return elements
  }

  const updatedQuery = query.toLowerCase().split(" ");
  const searchedElements =[];
    elements.map((element) => {
    const { name } = element;
    const flag = updatedQuery.every((partQuery) =>
      (name.toLowerCase().indexOf(partQuery) !== -1)
    );
    if (flag){
      searchedElements.push(element)
    }
  });

  return searchedElements;
};

export {
  searchElements
}