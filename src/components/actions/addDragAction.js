import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import { alphabet } from "./sort/sortByName";
import elementsActions from "../../store/elements/elementsActions";

function addDragActions(ComposedComponent) {
  class DragActions extends Component {
    static propTypes = {
      children: PropTypes.node,
      elementsActions: PropTypes.objectOf(PropTypes.func).isRequired,
      leftElementsForSearch: PropTypes.arrayOf(PropTypes.any),
      rightElementsForSearch: PropTypes.arrayOf(PropTypes.any),
    };
    static defaultProps = {
      children: undefined,
      leftElementsForSearch: [],
      rightElementsForSearch: [],
      cachedLeftElements: undefined,
    };

    constructor(props) {
      super(props);
      this.getList = this.getList.bind(this);
      this.dragActions = {
        onDragEnd: this.onDragEnd.bind(this),
      };
    }

    static moveElement(source, destination, droppableSource, droppableDestination) {
      const sourceClone = Array.from(source);
      const destClone = Array.from(destination);
      const [removed] = sourceClone.splice(droppableSource.index, 1);

      destClone.splice(droppableDestination.index, 0, removed);

      const result = {};
      result[droppableSource.droppableId] = sourceClone;
      result[droppableDestination.droppableId] = destClone;

      return result;
    };

    getList(idListElements) {
      const { leftElementsForSearch, rightElementsForSearch } = this.props;
      return idListElements === "left" ? leftElementsForSearch : rightElementsForSearch;
    }

    onDragEnd(result) {
      const { elementsActions } = this.props;
      const { source, destination } = result;
      if (!destination) {
        return;
      }
      if (source.droppableId === destination.droppableId) {
        return;
      } else {
        const result = DragActions.moveElement(
          this.getList(source.droppableId),
          this.getList(destination.droppableId),
          source,
          destination
        );
        elementsActions.updateListsElements({
          leftElements: alphabet({ elements: result.left }),
          rightElements: result.right
        });

      }
    }

    render() {
      const { children, ...other } = this.props;

      return (
        <ComposedComponent
          dragActions={this.dragActions}
          {...other}
        >
          {children}
        </ComposedComponent>
      );
    }

  }

  function mapStateToProps(state) {
    return {
      leftElementsForSearch: state.elements.leftElementsForSearch,
      rightElementsForSearch: state.elements.rightElementsForSearch,
    };
  }

  function mapDispatchToProps(dispatch) {
    return {
      elementsActions: bindActionCreators(elementsActions, dispatch),
    };
  }


  return connect(mapStateToProps, mapDispatchToProps)(DragActions);
}

export default addDragActions;