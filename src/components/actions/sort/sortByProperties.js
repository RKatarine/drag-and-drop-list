const sortByProperties = ({ elements, properties }) => {
  if (properties.length !== 0) {
    return elements.filter((element) => {
      const { flags } = element;
      const filteredFlags = flags.filter((flag) => properties.indexOf(flag) !== -1);
      return filteredFlags.length === properties.length
    })
  }
  return elements
};

export default sortByProperties;