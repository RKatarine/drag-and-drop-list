const alphabet = ({ elements }) => {
  return elements.sort((a, b) => {
    const nameA = `${a.name}`;
    const nameB = `${b.name}`;
    return nameA.localeCompare(nameB, "ru-Latn")
  });
};

const alphabetRevers = ({ elements }) => {
  return elements.sort((a, b) => {
    const nameA = `${a.name}`;
    const nameB = `${b.name}`;
    return nameB.localeCompare(nameA, "ru-Latn")
  });
};

export {
  alphabet,
  alphabetRevers
}