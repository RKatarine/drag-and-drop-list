import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import elementsActions from "../../store/elements/elementsActions";
import { searchElements } from "./search"
import { alphabet, alphabetRevers } from "./sort/sortByName";
import sortByProperties from "./sort/sortByProperties";

function addActions(ComposedComponent) {
  class Actions extends Component {
    static propTypes = {
      children: PropTypes.node,
      elementsActions: PropTypes.objectOf(PropTypes.func).isRequired,
      leftElementsForSearch: PropTypes.arrayOf(PropTypes.any),
      rightElementsForSearch: PropTypes.arrayOf(PropTypes.any),
      leftElements: PropTypes.arrayOf(PropTypes.any),
      rightElements: PropTypes.arrayOf(PropTypes.any),
    };
    static defaultProps = {
      children: undefined,
      leftElementsForSearch: [],
      rightElementsForSearch: [],
      leftElements: [],
      rightElements: [],
    };

    constructor(props) {
      super(props);
      this.commonActions = {
        getLeftElements: this.getLeftElements.bind(this),
        getRightElements: this.getRightElements.bind(this),
        selectRightElement: this.selectRightElement.bind(this),
        selectLeftElement: this.selectLeftElement.bind(this),
        searchElement: this.searchElement.bind(this),
        sortByName: this.sortByName.bind(this),
        sortByProperties: this.sortByProperties.bind(this),
      };
    }


    static getElementByName(selectedId, elements) {
      const index = selectedId.replace("leftId&", "").replace("rightId&", "");
      const result = elements[index];
      return result ? result : undefined;
    }

    searchElement(query, flag) {
      const {
        elementsActions,
        leftElementsForSearch,
        rightElementsForSearch
      } = this.props;
      if (flag === "left") {
        const searchedElements = searchElements(query, leftElementsForSearch);
        elementsActions.searchLeft(searchedElements, query);
      }
      if (flag === "right") {
        const searchedElements = searchElements(query, rightElementsForSearch);
        elementsActions.searchRight(searchedElements, query);
      }

    }

    sortByName(sortLeftElements) {
      const { elementsActions, leftElements } = this.props;
      const sortedLeftElements = sortLeftElements ? alphabet({ elements: leftElements }) :
        alphabetRevers({ elements: leftElements });
      elementsActions.sortLeftElements({ sortedLeftElements, sortLeftElements });
    }

    sortByProperties(propertiesForSort) {
      const { elementsActions, rightElementsForSearch } = this.props;
      if (propertiesForSort.length) {
        const sortedRightElements = sortByProperties({
          elements: rightElementsForSearch,
          properties: propertiesForSort
        });
        elementsActions.sortRightElements({ sortedRightElements, propertiesForSort });
      } else {
        elementsActions.sortRightElements({ sortedRightElements:rightElementsForSearch, propertiesForSort });
      }
    }

    getLeftElements() {
      const { elementsActions } = this.props;
      elementsActions.getLeftElements();
    }

    getRightElements() {
      const { elementsActions } = this.props;
      elementsActions.getRightElements();
    }

    selectLeftElement(selectedId) {
      const { elementsActions, leftElements } = this.props;
      const selectedItem = Actions.getElementByName(selectedId, leftElements);
      elementsActions.selectElement({ selectedId, selectedItem });
    }

    selectRightElement(selectedId) {
      const { elementsActions, rightElements } = this.props;
      const selectedItem = Actions.getElementByName(selectedId, rightElements);
      elementsActions.selectElement({ selectedId, selectedItem });
    }

    render() {
      const { children, ...other } = this.props;

      return (
        <ComposedComponent
          commonActions={this.commonActions}
          {...other}
        >
          {children}
        </ComposedComponent>
      );
    }
  }

  function mapStateToProps(state) {
    return {
      leftElements: state.elements.leftElements,
      rightElements: state.elements.rightElements,
      leftElementsForSearch: state.elements.leftElementsForSearch,
      rightElementsForSearch: state.elements.rightElementsForSearch,
    };
  }

  function mapDispatchToProps(dispatch) {
    return {
      elementsActions: bindActionCreators(elementsActions, dispatch),
    };
  }


  return connect(mapStateToProps, mapDispatchToProps)(Actions);
}

export default addActions;
