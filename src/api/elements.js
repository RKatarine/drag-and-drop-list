import generateData from "./responseData";

export default function elementsApi(config, axios) {
  function getAllLeft() {
    return Promise.resolve(JSON.parse(JSON.stringify({
      data: {
        data: generateData(),
        status: true,
        statusText: "Imitation getting elements from server"
      }
    })))
  }
  function getAllRight() {
    return Promise.resolve(JSON.parse(JSON.stringify({
      data: {
        data: generateData(),
        status: true,
        statusText: "Imitation getting elements from server"
      }
    })))
  }

  return {
    getAllLeft,
    getAllRight
  };
}
