import { flags } from "../constants/flags";

function randomInteger(min, max) {
  let rand = min + Math.random() * (max + 1 - min);
  return Math.floor(rand);
}

function generateData() {
  const result = [];
  for (let i = 0; i < 100; i++) {
    let name = `item_${i}`;
    const countFlags = randomInteger(1, 3);
    let setFlags = [];
    for (let j = 0; j < countFlags; j++) {
      setFlags.push(flags[j])
    }
    result.push({ name, flags: setFlags })
  }
  return result;
}

export default generateData;